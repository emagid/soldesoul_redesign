<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

        <div class="hero hero_sm" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/hero_inner.jpg')">
                        <div class="cta">
                <h2><?php the_title(); ?></h2>
            </div>
        </div>

        <div class="inner_content"> 
                <div class="top_portion">
                    <div class="blog_container">
                   <?php
	  			$args = array(
	    		'post_type' => 'blogs'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>            
                

                        <div class="blog_box">
                            <a href="<?php the_permalink(); ?>">
                            <div class="blog_box_bg" style="background-image:url('<?php the_field('image'); ?>')">
                            </div>
                            <div class="blog_box_content">
                                <h2><?php the_title(); ?></h2>
                                <p><?php the_field('intro_sentence'); ?>..<strong> Read More</strong></p>
                            </div>
                            </a>
                        </div>

            
                <?php
			}
				}
			else {
			echo 'No Blogs Found';
			}
		?>  
                </div>
            </div>
        </div>

<?php
get_sidebar();
get_footer();
