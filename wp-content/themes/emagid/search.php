<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package emagid
 */

get_header(); ?>

        <div class="hero hero_sm" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/cat_banner.jpg')">
            <div class="overlay">
                <div class="cta">
                    <h2><?php woocommerce_page_title(); ?></h2>
                </div>
            </div>
        </div>


<div class="woo_archive search">
		<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php
					/* translators: %s: search query. */
					printf( esc_html__( 'Search Results for: %s', 'emagid' ), '<span>' . get_search_query() . '</span>' );
				?></h1>
			</header><!-- .page-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

</div>

<?php
get_footer();
