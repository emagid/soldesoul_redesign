<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>

	</div><!-- .site_reset -->

	<footer class="site_footer">
        <div class="wrapper">
            <div class="footer_menus" id="desktop">
                <div>
                    <h5>The Brand</h5>
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'footer-brand'
                        ) );
                    ?>
                </div>
                                <div>
                    <h5>Women</h5>
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'footer-women'
                        ) );
                    ?>
                </div>
                                <div>
                    <h5>Men</h5>
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'footer-men'
                        ) );
                    ?>
                </div>
                                <div>
                    <h5>Collections</h5>
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'footer-collections'
                        ) );
                    ?>
                </div>
                                <div>
                    <h5>Policies</h5>
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'footer-policies'
                        ) );
                    ?>
                </div>
                <div>
                    <h5>Education</h5>
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'footer-edu'
                        ) );
                    ?>
                    <div class="footer_social">
                        <a href="https://www.facebook.com/soldesoul" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/fb_small.png">
                        </a>
                        <a href="https://www.instagram.com/soldesouljewelry/" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ig_small.png">
                        </a>
                                    
            </div>
                </div>
                
            </div>
            <div class="footer_menus" id="mobile">
                <div>
                    <?php
                        wp_nav_menu( array(
                            'theme_location' => 'footer-mobile'
                        ) );
                    ?>
                
                </div>
                <div class="footer_social">
                        <a href="https://www.facebook.com/soldesoul" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/fb_small.png">
                        </a>
                        <a href="https://www.instagram.com/soldesouljewelry/" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ig_small.png">
                        </a>
                                    
            </div>
            </div>
        </div>
	</footer>

<!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
window.__lc = window.__lc || {};
window.__lc.license = 9667340;
(function() {
  var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
  lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
  var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
})();
</script>
<!-- End of LiveChat code -->


<!--LUCKY ORANGE SCRIPT-->
<script type='text/javascript'>
window.__lo_site_id = 104472;

	(function() {
		var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
		wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
	  })();
	</script>

<?php wp_footer(); ?>

</body>
</html>
