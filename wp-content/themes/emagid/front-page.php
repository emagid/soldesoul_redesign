<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>


<div class="page_home">

    
  <div class="swiper-container">
    <div class="swiper-scrollbar"></div>
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
    <div class="swiper-wrapper">
        
                <?php
	  			$args = array(
	    		'post_type' => 'home_banners'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?> 
  
      <div class="swiper-slide">    
          <div>
            
                 <div class=" hero hero_lg" style="background-image:url('<?php the_field('banner'); ?>')">
                    <a href="<?php the_field('link'); ?>">
                    <div class="cta">
                        <h2><?php the_field('banner_header'); ?></h2>
                        <p><?php the_field('banner_text'); ?></p>
                        <h4><?php the_field('banner_cta'); ?></h4>
                    </div>
                        </a>

                </div> 

            
            </div>
        
        </div>
        
        <?php
			}
				}
			else {
			echo 'No Projects Found';
			}
		?>

    </div>
    <div class="swiper-pagination"></div>
  </div>
  <script src="<?php echo get_template_directory_uri(); ?>/assets/swiper/js/swiper.min.js"></script>
  <script>
    var swiper = new Swiper('.swiper-container', {
      spaceBetween: 50,
        loop: true,
      slidesPerView: 1,
          autoplay: {
    delay: 5000,
  },

    });
  </script>
<div class="products_featured">
    <div class="wrapper">
    <h2>Our Popular Picks</h2>
        <div class="picks">
    <?php echo do_shortcode('[featured_products per_page="3" columns="3"]'); ?>
            </div>
                <a href="/category/women/necklaces/" class="button_std">Shop More</a>
        </div>
</div>
    <div class="product_collections">
        <div class="explore">
                <a href="/category/collections/d-colony/">
        <div class="hero hero_med" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/ring_collection1a.jpg')">
            <h2 class="purp">Explore Our Collection</h2>
        </div>
    </a>
        </div>

        
        <div class="collect_four">
                <a href="/category/collections/be-hive/">
        <div class="hero hero_med" style="background-image:url('https://soldesoul.com/wp-content/uploads/2017/08/XI5A6387-1200x300.jpg')">
            <div class="overlay">
                <h2>Be-Hive Yourself</h2>
                <p>Bees have an absolute obsession with what is good for the whole hive over what is good for any one bee. <br>Every bee is committed to the greatest purpose of the hive...</p>
            </div>
            
        </div>
    </a>
                <a href="/category/collections/gold-network/">
        <div class="hero hero_med" style="background-image:url('https://soldesoul.com/wp-content/uploads/2017/08/XI5A6602-1200x300.jpg')">
            <div class="overlay">
            
            <h2>Gold Network</h2>
            <p>The top winners in any sporting event get the gold medal. <br> That’s because gold represents success, top achievement, winning, and accomplishment...</p>
        </div>
            </div>
    </a>
                <a href="/category/collections/d-colony/">
        <div class="hero hero_med" style="background-image:url('https://soldesoul.com/wp-content/uploads/2017/08/XI5A6506-1200x300.jpg')">
            <div class="overlay">
            <h2>D-Colony</h2>
                <p>With sturdy structures, formed in earth’s deep surface and with special resources from nature, diamonds are the strongest minerals in existence...</p>
            </div>
        </div>
    </a>
                <a href="/category/collections/chain-chain-chain/">
        <div class="hero hero_med" style="background-image:url('https://soldesoul.com/wp-content/uploads/2017/08/XI5A6690-1200x300.jpg')">
            <div class="overlay">
            <h2>Chain Chain Chain</h2>
                <p>Our life starts with connection.
First, with our umbilical cord conected to our mother, later on with her eye contact and our family’s soft hugs and warmth...</p>
            </div>
        </div>
    </a>
        </div>
        </div>
    <a href="/category/sale/">
        <div class="hero hero_sm" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/ring_collection.jpg')">
            <h2 class="purp">Items on Sale</h2>
        </div>
    </a>
    
    <div class="our_brand">
        <h2>Our Brand</h2>
        <p>We have four leading values of our company.</p>
        <ul>
            <li>Joy of Life</li>
            <li>Creativity</li>
            <li>Quality</li>
            <li>Belonging</li>
        </ul>
        <a href="/our-vision-values/" class="button_std">Find Out More</a>
    </div>
    
    
    <div class="social_wall">
<!--
        <div class="social_wall_icons">
            <a href="https://www.facebook.com/soldesoul" target="_blank">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/fb_pink.png">
            </a>
            <a href="https://www.instagram.com/soldesouljewelry/" target="_blank">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/ig_pink.png">
            </a>
        </div>
-->
    <?php echo do_shortcode('[instagram-feed]'); ?>
        
<!--
        <div class="mobile_hide">
    <//?php echo do_shortcode('[instagram-feed]'); ?>
        </div>
-->
    </div>


</div>

<script>
    $(".explore").hover(function(){
    $(this).fadeOut("slow");
    }, function(){
    $(".collect_four").show("slow");
});
</script>
<?php
get_sidebar();
get_footer();
