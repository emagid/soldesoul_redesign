<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

        <div class="hero hero_sm" style="background-image:url('<?php the_field('banner_image'); ?>')">
                        <div class="cta">
                <h2><?php the_title(); ?></h2>
            </div>
        </div>

        <div class="inner_content contact_content"> 
            <div class="contact_container">
                <div class="header">
                    <h3>We listen to our customer's needs and feedback.</h3>
                </div>
                <div class="contact_board">
                    <div class="board">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/map-pin.png">
                        <h4>Address</h4>
                        <p>643 S. Olive Street #530<br>Los Angeles, California, 90014</p>
                    </div>
                    <div class="board">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/email.png">
                        <h4>Email</h4>
                        <p>info@soldesoul.com</p>
                    </div>
                    <div class="board">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/smartphone.png">
                        <h4>Phone</h4>
                        <p>1-213-221-7245</p>
                    </div>
                </div>
                <div class="header">
                    <h3>We are here to help</h3>
                    <h2>Need some help? Fill out the form below and our staff will be in touch!</h2>
                                    <div class="contact_form">
                    <?php echo do_shortcode('[contact-form-7 id="4" title="Contact form 1"]'); ?>
                </div>
                </div>

            </div>
            
        </div>

<?php
get_sidebar();
get_footer();
