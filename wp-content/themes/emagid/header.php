<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package emagid
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    
<!--CSS-->    
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/redesign.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/assets/css/media.css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,600|https://fonts.googleapis.com/css?family=Merriweather:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/swiper/css/swiper.min.css">
    
<!--    JS-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/assets/js/jquery.sticky.js"></script>
    
    


	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<header class="site_header">
        <div class="wrapper">
            <div class="branding">
                <a href="/">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/site_logo.png">
                </a>
            </div>

            <nav class="site_navigation">
                <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-1'
                    ) );
                ?>
            </nav>
            <div class="live_search">
                
                <div class="search_field">
                    <?php echo do_shortcode('[wpbsearch]'); ?>
                </div>
            </div>
        </div>
	</header>
        <div class="site_reset">