<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

        <div class="hero hero_sm" style="background-image:url('<?php the_field('banner_image'); ?>')">
                        <div class="cta">
                <h2><?php the_title(); ?></h2>
            </div>
        </div>

        <div class="inner_content"> 

                
                
                <div class="top_portion">
                    <h1><?php the_title(); ?></h1>
                    <?php the_field('top_portion'); ?>
                </div>
                <div class="lower_portion">
                    <div class="wrapper">
                        <?php the_field('lower_portion'); ?>
                    </div>
                </div>

        </div>

            </div>
        </div>

<?php
get_sidebar();
get_footer();
