<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package emagid
 */

get_header(); ?>

        <div class="hero hero_sm" style="background-image:url('<?php echo get_template_directory_uri(); ?>/assets/img/hero_inner.jpg')">
        </div>

        <div class="inner_content"> 

                
                
                <div class="top_portion">
                    <h1><?php the_title(); ?></h1>
                    <?php the_field('top_portion'); ?>
                </div>
                <div class="lower_portion">
                    <div class="wrapper">
                        <?php the_field('lower_portion'); ?>
                    </div>
                </div>

        </div>

<?php
get_sidebar();
get_footer();
